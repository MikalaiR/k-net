import {MigrationInterface, QueryRunner} from "typeorm";
import * as faker from 'faker';
import {UserEntity} from '../modules/employees/user.entity';
import {PasswordTransformer} from '../modules/employees/password.transformer';
import {Client} from '../modules/clients/entities/client.entity';

faker.setLocale('ru');

export class Fakedata1570007936100 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        for (let i = 0; i < 500; ++i) {
            const user = new Client();
            user.firstName = faker.name.firstName();
            user.lastName = faker.name.lastName();
            user.email = faker.internet.email(user.firstName, user.lastName);
            user.phone = faker.phone.phoneNumber();
            user.password = new PasswordTransformer().to(faker.internet.password());
            user.passport = {};
            await queryRunner.query(`INSERT INTO client (first_name, last_name, email, password, phone, passport)
                                     VALUES ($1, $2, $3, '', $4, $5)`,
                [user.firstName, user.lastName, user.email, user.phone, user.passport]);
        }

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`TRUNCATE TABLE clients;`);
    }

}
