import {MigrationInterface, QueryRunner} from "typeorm";

export class SessionStore1570136101098 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "clients" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "middle_name" character varying, "passport" jsonb NOT NULL, "phone" character varying NOT NULL, "email" character varying, "password" character varying NOT NULL, CONSTRAINT "PK_f1ab7cf3a5714dbc6bb4e1c28a4" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "sessions" ("id" SERIAL NOT NULL, "start" TIMESTAMP NOT NULL, "end" TIMESTAMP NOT NULL, "traffic" bigint NOT NULL, "contract_id" integer, CONSTRAINT "PK_3238ef96f18b355b671619111bc" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "available"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "type"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "type" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "available" boolean NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "price"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "price" money NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "price"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "price" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "sessions" ADD CONSTRAINT "FK_01a9cf25bb44b4102b797153de2" FOREIGN KEY ("contract_id") REFERENCES "contract"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "sessions" DROP CONSTRAINT "FK_01a9cf25bb44b4102b797153de2"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "price"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "price" money NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "price"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "price" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "available"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "type"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "type" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "available" boolean NOT NULL`, undefined);
        await queryRunner.query(`DROP TABLE "sessions"`, undefined);
        await queryRunner.query(`DROP TABLE "clients"`, undefined);
    }

}
