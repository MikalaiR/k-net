import './boilerplate.polyfill';

import {TypeOrmModule} from '@nestjs/typeorm';
import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';

import {ConfigService} from './shared/services/config.service';
import {AuthModule} from './modules/auth/auth.module';
import {UserModule} from './modules/employees/user.module';
import {SharedModule} from './shared.module';
import {contextMiddleware} from './middlewares/context.middelware';
import {ClientsModule} from './modules/clients/clients.module';
import {ContractsModule} from './modules/contracts/contracts.module';
import {ServicesModule} from './modules/services/services.module';
import {TariffsModule} from './modules/tariffs/tariffs.module';
import {TransactionsModule} from './modules/transactions/transactions.module';
import {SessionsModule} from './modules/sessions/sessions.module';
import {NbSecurityModule} from "./security/security.module";
import {APP_GUARD, APP_INTERCEPTOR} from "@nestjs/core";
import {RolesGuard} from "./guards/roles.guard";
import {AuthGuard} from "./guards/auth.guard";
import {AuthUserInterceptor} from "./interceptors/auth-user-interceptor.service";

@Module({
    imports: [
        AuthModule,
        UserModule,
        ClientsModule,
        ContractsModule,
        ServicesModule,
        TariffsModule,
        TransactionsModule,
        TypeOrmModule.forRootAsync({
            imports: [SharedModule],
            useFactory: (configService: ConfigService) => configService.typeOrmConfig,
            inject: [ConfigService],
        }),
        SessionsModule
    ],
    providers: [
        {
            provide: APP_GUARD,
            useClass: AuthGuard,
        },
        {
            provide: APP_GUARD,
            useClass: RolesGuard,
        },
        {
            provide: APP_INTERCEPTOR,
            useClass: AuthUserInterceptor
        }
    ]
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
        consumer.apply(contextMiddleware).forRoutes('*');
    }
}
