export enum RoleType {
    Client = 'client',
    Admin = 'admin',
    Manager = 'manager'
}
