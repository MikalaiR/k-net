import { SetMetadata } from '@nestjs/common';
import { RoleType } from '../constants/role-type';

export const Roles = (...roles: RoleType[]) => SetMetadata('roles', roles);

export const Permissions = (...permissions: string[]) => SetMetadata('permissions', permissions);

export const SkipAuth = () => SetMetadata('skip_auth', true);
