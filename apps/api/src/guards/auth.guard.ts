import { AuthGuard as NestAuthGuard } from '@nestjs/passport';
import {UnauthorizedException} from "@nestjs/common";

export class AuthGuard extends NestAuthGuard('jwt') {
    handleRequest(err, user, info, context): any {
        if (err) {
            throw err;
        }
        return user;
    }
}
