import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {UserEntity} from '../modules/employees/user.entity';
import {NbAclService} from "../security/services/acl.service";
import {AuthService} from "../modules/auth/auth.service";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private readonly _reflector: Reflector,
                private readonly _sec: NbAclService) {
    }

    canActivate(context: ExecutionContext): boolean {
        const perm: string[] = this._reflector.get<string[]>('permissions', context.getHandler());

        if (!perm) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const user = <UserEntity>request.user;

        const role = user ? user.role : 'guest';

        // @ts-ignore
        return this._sec.can(role, ...perm);

        // return perm.includes(user.role);
    }
}
