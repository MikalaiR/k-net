import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1570197176737 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "clients" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "middle_name" character varying, "passport" jsonb NOT NULL, "phone" character varying NOT NULL, "email" character varying, "password" character varying NOT NULL, CONSTRAINT "PK_f1ab7cf3a5714dbc6bb4e1c28a4" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "tariff" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "price" money NOT NULL, "type" integer NOT NULL, "extra" jsonb NOT NULL, CONSTRAINT "PK_bbeac9df199ea1c22c6dea75c2f" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "service" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "title" character varying NOT NULL, "description" character varying NOT NULL, "price" integer NOT NULL, "extra" jsonb NOT NULL, CONSTRAINT "PK_85a21558c006647cd76fdce044b" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "contract" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "balance" money NOT NULL, "credentials" jsonb NOT NULL, "client_id" integer, "tariff_id" integer, CONSTRAINT "PK_17c3a89f58a2997276084e706e8" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "sessions" ("id" SERIAL NOT NULL, "start" TIMESTAMP NOT NULL, "end" TIMESTAMP NOT NULL, "traffic" bigint NOT NULL, "contract_id" integer, CONSTRAINT "PK_3238ef96f18b355b671619111bc" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "transaction" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "amount" money NOT NULL, "description" character varying NOT NULL, "extra" jsonb NOT NULL, "contract_id" integer, CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab9" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TYPE "users_role_enum" AS ENUM('USER', 'ADMIN')`, undefined);
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "first_name" character varying, "last_name" character varying, "role" "users_role_enum" NOT NULL DEFAULT 'USER', "email" character varying, "password" character varying, "phone" character varying, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "contract_services" ("contract_id" integer NOT NULL, "service_id" integer NOT NULL, CONSTRAINT "PK_dfad6ff923a3880da73ae7274c2" PRIMARY KEY ("contract_id", "service_id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_95fad3975dfa4ce4057e318bda" ON "contract_services" ("contract_id") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_1b80c9746f23e6c053d8144697" ON "contract_services" ("service_id") `, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "type"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "type" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "available" boolean NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "price"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "price" integer NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "contract" ADD CONSTRAINT "FK_17598b1610ffe62285e28e8679c" FOREIGN KEY ("client_id") REFERENCES "clients"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "contract" ADD CONSTRAINT "FK_0bdea111622f6ce5a116a7fa777" FOREIGN KEY ("tariff_id") REFERENCES "tariff"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "sessions" ADD CONSTRAINT "FK_01a9cf25bb44b4102b797153de2" FOREIGN KEY ("contract_id") REFERENCES "contract"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "transaction" ADD CONSTRAINT "FK_5a6888faa99bec2c7708d3e7e1a" FOREIGN KEY ("contract_id") REFERENCES "contract"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "contract_services" ADD CONSTRAINT "FK_95fad3975dfa4ce4057e318bda0" FOREIGN KEY ("contract_id") REFERENCES "contract"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "contract_services" ADD CONSTRAINT "FK_1b80c9746f23e6c053d81446972" FOREIGN KEY ("service_id") REFERENCES "service"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "contract_services" DROP CONSTRAINT "FK_1b80c9746f23e6c053d81446972"`, undefined);
        await queryRunner.query(`ALTER TABLE "contract_services" DROP CONSTRAINT "FK_95fad3975dfa4ce4057e318bda0"`, undefined);
        await queryRunner.query(`ALTER TABLE "transaction" DROP CONSTRAINT "FK_5a6888faa99bec2c7708d3e7e1a"`, undefined);
        await queryRunner.query(`ALTER TABLE "sessions" DROP CONSTRAINT "FK_01a9cf25bb44b4102b797153de2"`, undefined);
        await queryRunner.query(`ALTER TABLE "contract" DROP CONSTRAINT "FK_0bdea111622f6ce5a116a7fa777"`, undefined);
        await queryRunner.query(`ALTER TABLE "contract" DROP CONSTRAINT "FK_17598b1610ffe62285e28e8679c"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "price"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "price" money NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "available"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" DROP COLUMN "type"`, undefined);
        await queryRunner.query(`ALTER TABLE "tariff" ADD "type" integer NOT NULL`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_1b80c9746f23e6c053d8144697"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_95fad3975dfa4ce4057e318bda"`, undefined);
        await queryRunner.query(`DROP TABLE "contract_services"`, undefined);
        await queryRunner.query(`DROP TABLE "users"`, undefined);
        await queryRunner.query(`DROP TYPE "users_role_enum"`, undefined);
        await queryRunner.query(`DROP TABLE "transaction"`, undefined);
        await queryRunner.query(`DROP TABLE "sessions"`, undefined);
        await queryRunner.query(`DROP TABLE "contract"`, undefined);
        await queryRunner.query(`DROP TABLE "service"`, undefined);
        await queryRunner.query(`DROP TABLE "tariff"`, undefined);
        await queryRunner.query(`DROP TABLE "clients"`, undefined);
    }

}
