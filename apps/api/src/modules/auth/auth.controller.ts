import {Body, Controller, Get, HttpCode, HttpStatus, Post, UseGuards, UseInterceptors} from '@nestjs/common';
import {ApiBearerAuth, ApiOkResponse, ApiUseTags} from '@nestjs/swagger';

import {AuthUser} from '../../decorators/auth-user.decorator';
import {AuthGuard} from '../../guards/auth.guard';
import {AuthUserInterceptor} from '../../interceptors/auth-user-interceptor.service';
import {UserEntity} from '../employees/user.entity';
import {UserService} from '../employees/user.service';
import {AuthService} from './auth.service';
import {LoginPayloadDto} from './dto/LoginPayloadDto';
import {UserLoginDto} from './dto/UserLoginDto';
import {UserRegisterDto} from './dto/UserRegisterDto';

@Controller('auth')
@ApiUseTags('auth')
export class AuthController {

    constructor(
        public readonly userService: UserService,
        public readonly authService: AuthService,
    ) {
    }

    @Post('login')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({type: LoginPayloadDto, description: 'User info with access token'})
    async userLogin(@Body() userLoginDto: UserLoginDto): Promise<LoginPayloadDto> {
        const userEntity = await this.authService.validateUser(userLoginDto);

        const [user, token] = await Promise.all([userEntity, this.authService.createToken(userEntity)]);
        return new LoginPayloadDto(user, token);
    }

    @Post('register')
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({type: UserEntity, description: 'Successfully Registered'})
    async userRegister(
        @Body() userRegisterDto: UserRegisterDto,
    ): Promise<UserEntity> {
        return this.userService.createUser(userRegisterDto);
    }

    @Get('me')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard)
    @UseInterceptors(AuthUserInterceptor)
    @ApiBearerAuth()
    @ApiOkResponse({type: UserEntity, description: 'current user info'})
    getCurrentUser(@AuthUser() user: UserEntity) {
        return user;
    }
}

