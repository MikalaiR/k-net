import { TokenPayloadDto } from './TokenPayloadDto';
import { ApiModelProperty } from '@nestjs/swagger';
import {UserEntity} from '../../employees/user.entity';

export class LoginPayloadDto {
    @ApiModelProperty({ type: UserEntity })
    user: UserEntity;
    @ApiModelProperty({ type: TokenPayloadDto })
    token: TokenPayloadDto;

    constructor(user: UserEntity, token: TokenPayloadDto) {
        this.user = user;
        this.token = token;
    }
}
