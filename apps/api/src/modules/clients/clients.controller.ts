import {Controller, UseGuards} from '@nestjs/common';
import {ClientsService} from './clients.service';
import {Crud} from '@nestjsx/crud';
import {Client} from './entities/client.entity';
import {Permissions, Roles} from '../../decorators/roles.decorator';
import {RoleType} from '../../constants/role-type';
import {ApiBearerAuth, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from "../../guards/auth.guard";
import {RolesGuard} from "../../guards/roles.guard";

@Crud({
    model: {
        type: Client,
    },
    query: {

    },
    routes: {
        only: ['getManyBase', 'getOneBase', 'updateOneBase', 'createOneBase'],
        getManyBase: {
            decorators: [Permissions('view', 'clients')]
        },
        getOneBase: {
            decorators: [Permissions('view', 'clients')]
        },
        updateOneBase: {
            decorators: [Permissions('edit', 'clients')]
        },
        createOneBase: {
            decorators: [Permissions('create', 'clients')]
        }
    }
})
@ApiUseTags('clients')
@ApiBearerAuth()
@Controller('clients')
export class ClientsController {
    constructor(private service: ClientsService) {
    }
}
