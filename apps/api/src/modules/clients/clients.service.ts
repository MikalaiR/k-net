import {Injectable} from '@nestjs/common';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm';
import {Client} from './entities/client.entity';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';

@Injectable()
export class ClientsService extends TypeOrmCrudService<Client> {
    constructor(@InjectRepository(Client) clientsRepository: Repository<Client>) {
        super(clientsRepository);
    }
}
