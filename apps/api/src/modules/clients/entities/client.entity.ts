import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    ManyToOne,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn
} from 'typeorm';
import {IsEmail, IsJSON, IsOptional, IsPhoneNumber, IsString} from 'class-validator';
import {ApiModelProperty} from '@nestjs/swagger';
import {PasswordTransformer} from '../../employees/password.transformer';
import {IsPassword} from '../../../decorators/is-password.validator';
import {AbstractEntity} from '../../../common/abstract.entity';
import {UserEntity} from "../../employees/user.entity";

@Entity('clients')
export class Client extends AbstractEntity<Client> {
    @ApiModelProperty()
    @IsString()
    @Column()
    firstName: string;

    @ApiModelProperty()
    @IsString()
    @Column()
    lastName: string;

    @ApiModelProperty({required: false})
    @IsString()
    @IsOptional()
    @Column({nullable: true})
    middleName: string | null;

    @IsJSON()
    @ApiModelProperty({required: false})
    @Column({type: 'jsonb'})
    passport: object;

    @ApiModelProperty({required: true})
    @IsPhoneNumber('BY')
    @Column()
    phone: string;

    @Index({ unique: true })
    @ApiModelProperty({required: true})
    @IsEmail()
    @Column({nullable: true})
    email: string;

    @IsPassword()
    @ApiModelProperty({required: true})
    @Column({transformer: new PasswordTransformer()})
    password: string;

    @ManyToOne(() => UserEntity)
    personalManager: UserEntity;
}
