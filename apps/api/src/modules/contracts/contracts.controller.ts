import {Controller} from '@nestjs/common';
import {Crud} from '@nestjsx/crud';
import {Contract} from './entities/contract.entity';
import {ContractsService} from './contracts.service';
import {ApiUseTags} from '@nestjs/swagger';
import {Permissions} from "../../decorators/roles.decorator";

@Crud({
    model: {
        type: Contract,
    },
    query: {
        join: {
            client: {},
        },
    },
    routes: {
        only: ['getOneBase', 'getManyBase', 'updateOneBase', 'createOneBase'],
        getManyBase: {
            decorators: [Permissions('view', 'contracts')]
        },
        getOneBase: {
            decorators: [Permissions('view', 'contracts')]
        },
        updateOneBase: {
            decorators: [Permissions('edit', 'contracts')]
        },
        createOneBase: {
            decorators: [Permissions('create', 'contracts')]
        }
    }
})
@ApiUseTags('contracts')
@Controller('contracts')
export class ContractsController {
    constructor(private service: ContractsService) {
    }

}
