import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm';
import {Contract} from './entities/contract.entity';
import {Repository} from 'typeorm';

@Injectable()
export class ContractsService extends TypeOrmCrudService<Contract> {
    constructor(@InjectRepository(Contract) contractsRepository: Repository<Contract>) {
        super(contractsRepository);
    }
}
