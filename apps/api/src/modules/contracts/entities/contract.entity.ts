import {Column, Entity, JoinTable, ManyToMany, ManyToOne} from 'typeorm';
import {Client} from '../../clients/entities/client.entity';
import {Tariff} from '../../tariffs/entities/tariff.entity';
import {Service} from '../../services/entities/service.entity';
import {AbstractEntity} from '../../../common/abstract.entity';
import {ApiModelProperty} from '@nestjs/swagger';

@Entity({
    name: 'contracts'
})
export class Contract extends AbstractEntity<Contract> {
    @ManyToOne(() => Client)
    client: Client;

    @ManyToOne(() => Tariff)
    tariff: Promise<Tariff>;

    @ApiModelProperty()
    @Column({type: 'money'})
    balance: number;

    @ApiModelProperty()
    @Column({type: 'jsonb'})
    credentials: object;

    @ManyToMany(() => Service)
    @JoinTable({name: 'contract_services'})
    services: Service[];
}
