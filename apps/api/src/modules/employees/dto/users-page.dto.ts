import {ApiModelProperty} from '@nestjs/swagger';
import {PageMetaDto} from '../../../common/dto/PageMetaDto';
import {UserEntity} from '../user.entity';

export class UsersPageDto {
    @ApiModelProperty({
        type: UserEntity,
        isArray: true,
    })
    readonly data: UserEntity[];

    @ApiModelProperty()
    readonly meta: PageMetaDto;

    constructor(data: UserEntity[], meta: PageMetaDto) {
        this.data = data;
        this.meta = meta;
    }
}
