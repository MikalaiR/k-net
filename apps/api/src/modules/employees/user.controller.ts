import {Controller, Get, HttpCode, HttpStatus, Query, UseGuards, UseInterceptors, ValidationPipe} from '@nestjs/common';
import {ApiBearerAuth, ApiResponse, ApiUseTags} from '@nestjs/swagger';

import {RoleType} from '../../constants/role-type';
import {AuthUser} from '../../decorators/auth-user.decorator';
import {Roles} from '../../decorators/roles.decorator';
import {AuthGuard} from '../../guards/auth.guard';
import {RolesGuard} from '../../guards/roles.guard';
import {AuthUserInterceptor} from '../../interceptors/auth-user-interceptor.service';
import {UsersPageOptionsDto} from './dto/users-page-options.dto';
import {UsersPageDto} from './dto/users-page.dto';
import {UserEntity} from './user.entity';
import {UserService} from './user.service';
import {Crud} from '@nestjsx/crud';

@Crud({
    model: {
        type: UserEntity,
    },
    routes: {
        only: ['getOneBase', 'getManyBase'],
        getOneBase: {
            decorators: [
                Roles(RoleType.Admin)
            ],
        },
    },
})

@Controller('users')
@ApiUseTags('users')
@ApiBearerAuth()
export class UserController {

    constructor(private service: UserService) {
    }

    @Get('admin')
    @Roles(RoleType.Admin)
    @HttpCode(HttpStatus.OK)
    async admin(@AuthUser() user: UserEntity) {
        return 'only for you admin: ' + user.firstName;
    }
}
