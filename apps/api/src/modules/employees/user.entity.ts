import {Column, Entity} from 'typeorm';

import {AbstractEntity} from '../../common/abstract.entity';
import {RoleType} from '../../constants/role-type';
import {PasswordTransformer} from './password.transformer';
import {ApiModelPropertyOptional} from '@nestjs/swagger';
import {Exclude} from 'class-transformer';

@Entity({name: 'employees'})
export class UserEntity extends AbstractEntity<UserEntity> {
    @ApiModelPropertyOptional()
    @Column({nullable: true})
    firstName: string;

    @ApiModelPropertyOptional()
    @Column({nullable: true})
    lastName: string;

    @ApiModelPropertyOptional({enum: RoleType})
    @Column({type: 'enum', enum: RoleType, default: RoleType.Manager})
    role: RoleType;

    @ApiModelPropertyOptional()
    @Column({unique: true, nullable: true})
    email: string;

    @Exclude()
    @Column({nullable: true, transformer: new PasswordTransformer()})
    password: string;

    @ApiModelPropertyOptional()
    @Column({nullable: true})
    phone: string;
}
