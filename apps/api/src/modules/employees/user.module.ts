import {Module, forwardRef} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';

import {UserService} from './user.service';
import {UserController} from './user.controller';
import {AuthModule} from '../auth/auth.module';
import {UserEntity} from './user.entity';
import {NbSecurityModule} from "../../security/security.module";

@Module({
    imports: [
        forwardRef(() => AuthModule),
        TypeOrmModule.forFeature([UserEntity]),
    ],
    controllers: [UserController],
    exports: [UserService],
    providers: [
        UserService,
    ],
})
export class UserModule {
}
