import {Injectable} from '@nestjs/common';
import {FindConditions, QueryRunner, Repository, SelectQueryBuilder} from 'typeorm';
import {UserEntity} from './user.entity';
import {UserRegisterDto} from '../auth/dto/UserRegisterDto';
import {ValidatorService} from '../../shared/services/validator.service';
import {AwsS3Service} from '../../shared/services/aws-s3.service';
import {InjectRepository} from '@nestjs/typeorm';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm';

@Injectable()
export class UserService extends TypeOrmCrudService<UserEntity> {
    constructor(
        @InjectRepository(UserEntity) public readonly userRepository: Repository<UserEntity>,
        public readonly validatorService: ValidatorService,
        public readonly awsS3Service: AwsS3Service,
    ) {
        super(userRepository);
    }

    /**
     * Find single user
     */
    findUser(findData: FindConditions<UserEntity>): Promise<UserEntity> {
        return this.userRepository.findOne(findData);
    }

    /**
     * Find all users
     */
    findUsers(findData: FindConditions<UserEntity>): Promise<UserEntity[]> {
        return this.userRepository.find(findData);
    }

    createQueryBuilder(alias: string = 'user', queryRunner?: QueryRunner): SelectQueryBuilder<UserEntity> {
        return this.userRepository.createQueryBuilder(alias, queryRunner);
    }

    async findByUsernameOrEmail(options: { email: string }): Promise<UserEntity | undefined> {
        let queryBuilder = this.userRepository.createQueryBuilder('user');

        if (options.email) {
            queryBuilder = queryBuilder.orWhere('user.email = :email', {email: options.email});
        }

        return queryBuilder.getOne();
    }

    async createUser(userRegisterDto: UserRegisterDto): Promise<UserEntity> {
        const user = this.userRepository.create({...userRegisterDto});
        user.firstName = userRegisterDto.fullName.split(' ')[0];
        user.lastName = userRegisterDto.fullName.split(' ')[1];
        return this.userRepository.save(user);
    }
}
