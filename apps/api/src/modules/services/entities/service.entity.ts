import {Column, Entity} from 'typeorm';
import {AbstractEntity} from '../../../common/abstract.entity';

@Entity({
    name: 'services'
})
export class Service extends AbstractEntity<Service> {
    @Column()
    title: string;

    @Column()
    description: string;

    @Column({type: 'money'})
    price: number;

    @Column({type: 'jsonb'})
    extra: object;
}
