import {Controller} from '@nestjs/common';
import {ServicesService} from './services.service';
import {Crud} from '@nestjsx/crud';
import {Service} from './entities/service.entity';
import {Permissions} from "../../decorators/roles.decorator";

@Crud({
    model: {
        type: Service,
    },
    routes: {
        only: ['getManyBase', 'updateOneBase', 'createOneBase', 'updateOneBase'],
        getManyBase: {
            decorators: [Permissions('view', 'services')]
        },
        getOneBase: {
            decorators: [Permissions('view', 'services')]
        },
        updateOneBase: {
            decorators: [Permissions('edit', 'services')]
        },
        createOneBase: {
            decorators: [Permissions('create', 'services')]
        }
    },
    query: {
        exclude: ['extra']
    }
})
@Controller('services')
export class ServicesController {
    constructor(private service: ServicesService) {
    }
}
