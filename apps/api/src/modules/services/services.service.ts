import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Service} from './entities/service.entity';
import {Repository} from 'typeorm';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm';

@Injectable()
export class ServicesService extends TypeOrmCrudService<Service> {
    constructor(@InjectRepository(Service) servicesRepository: Repository<Service>) {
        super(servicesRepository);
    }

}
