import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Contract} from '../../contracts/entities/contract.entity';

@Entity('sessions')
export class Session {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Contract)
    contract: Contract;

    @Column({type: 'timestamp without time zone'})
    start: Date;

    @Column({type: 'timestamp without time zone'})
    end: Date;

    @Column({type: 'bigint'})
    traffic: number;
}
