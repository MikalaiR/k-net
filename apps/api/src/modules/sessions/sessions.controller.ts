import {Controller, Get} from '@nestjs/common';
import {SessionsService} from './sessions.service';
import {ApiUseTags} from "@nestjs/swagger";
import {Crud} from "@nestjsx/crud";
import {Session} from "./entities/session.entity";
import {Permissions} from "../../decorators/roles.decorator";

@Crud({
    model: {
        type: Session,
    },
    query: {
        exclude: ['extra'],
    },
    routes: {
        only: ['getManyBase'],
        getManyBase: {
            decorators: [Permissions('view', 'sessions')]
        }
    },
})
@ApiUseTags('sessions')
@Controller('sessions')
export class SessionsController {
    constructor(private service: SessionsService) {

    }

    @Get('weeklyStats')
    async weekStat() {
        return this.service.weeklyStats();
    }
}
