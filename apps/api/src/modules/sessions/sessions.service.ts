import {Injectable} from '@nestjs/common';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm';
import {Session} from './entities/session.entity';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';

@Injectable()
export class SessionsService extends TypeOrmCrudService<Session> {
    constructor(@InjectRepository(Session) private _sessionRepo: Repository<Session>) {
        super(_sessionRepo);
    }

    async weeklyStats(): Promise<any> {
        return this._sessionRepo.createQueryBuilder()
            .select('avg(traffic)')
			.addSelect('extract(isodow from start)', 'day')
            .groupBy('day')
            .getRawMany();
    }
}
