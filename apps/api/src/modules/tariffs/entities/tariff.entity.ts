import {Column, Entity} from 'typeorm';
import {AbstractEntity} from '../../../common/abstract.entity';
import {ApiModelProperty} from '@nestjs/swagger';

enum TariffType {
    Active = 'active',
    Archived = 'archived',
    Draft = 'draft',
}

@Entity({
    name: 'tariffs'
})
export class Tariff extends AbstractEntity<Tariff> {
    @ApiModelProperty()
    @Column()
    title: string;

    @ApiModelProperty()
    @Column()
    description: string;

    @ApiModelProperty()
    @Column({type: 'money'})
    price: number;

    @ApiModelProperty()
    @Column({enum: TariffType, type: 'enum', default: TariffType.Draft})
    type: TariffType;

    @ApiModelProperty()
    @Column({type: 'jsonb'})
    extra: object;
}
