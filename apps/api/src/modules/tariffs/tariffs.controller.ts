import {Controller} from '@nestjs/common';
import {Crud} from '@nestjsx/crud';
import {Tariff} from './entities/tariff.entity';
import {ApiUseTags} from "@nestjs/swagger";
import {TariffsService} from "./tariffs.service";

@Crud({
    model: {
        type: Tariff,
    },
    query: {
        exclude: ['extra'],
    },
    routes: {
        only: ['getManyBase', 'updateOneBase', 'createOneBase']
    },
})

@ApiUseTags('tariffs')
@Controller('tariffs')
export class TariffsController {
    constructor(private service: TariffsService) {}
}
