import {Module} from '@nestjs/common';
import {TariffsService} from './tariffs.service';
import {TariffsController} from './tariffs.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Tariff} from './entities/tariff.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Tariff])],
    providers: [TariffsService],
    controllers: [TariffsController],
})
export class TariffsModule {
}
