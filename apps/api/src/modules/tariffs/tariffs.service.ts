import {Injectable} from '@nestjs/common';
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {Tariff} from "./entities/tariff.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

@Injectable()
export class TariffsService extends TypeOrmCrudService<Tariff> {
    constructor(@InjectRepository(Tariff) private repository: Repository<Tariff>) {
        super(repository);
    }
}
