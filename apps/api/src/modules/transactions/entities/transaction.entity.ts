import {Column, Entity, ManyToOne} from 'typeorm';
import {Contract} from '../../contracts/entities/contract.entity';
import {AbstractEntity} from '../../../common/abstract.entity';

@Entity({
    name: 'transactions'
})
export class Transaction extends AbstractEntity<Transaction> {
    @ManyToOne(() => Contract)
    contract: Contract;

    @Column({type: 'money'})
    amount: number;

    @Column()
    description: string;

    @Column({type: 'jsonb'})
    extra: object;
}
