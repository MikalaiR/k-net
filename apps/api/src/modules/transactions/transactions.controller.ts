import {Controller} from '@nestjs/common';
import {Crud} from '@nestjsx/crud';
import {Tariff} from '../tariffs/entities/tariff.entity';
import {Transaction} from './entities/transaction.entity';
import {TransactionsService} from './transactions.service';
import {ApiUseTags} from "@nestjs/swagger";
import {Permissions} from "../../decorators/roles.decorator";

@Crud({
    model: {
        type: Transaction,
    },
    routes: {
        only: ['getManyBase'],
        getManyBase: {
            decorators: [Permissions('view', 'transactions')]
        }
    },
})
@ApiUseTags('transactions')
@Controller('transactions')
export class TransactionsController {
    constructor(private service: TransactionsService) {
    }
}
