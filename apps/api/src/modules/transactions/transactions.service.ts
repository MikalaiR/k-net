import {Injectable} from '@nestjs/common';
import {TypeOrmCrudService} from '@nestjsx/crud-typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {Transaction} from './entities/transaction.entity';

@Injectable()
export class TransactionsService extends TypeOrmCrudService<Transaction> {
    constructor(@InjectRepository(Transaction) transactionRepository: Repository<Transaction>) {
        super(transactionRepository);
    }
}
