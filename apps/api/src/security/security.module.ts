import {DynamicModule, Module} from '@nestjs/common';

import {NB_SECURITY_OPTIONS_TOKEN, NbAclOptions} from './security.options';
import {NbAclService} from './services/acl.service';

@Module({})
export class NbSecurityModule {
    static forRoot(nbSecurityOptions?: NbAclOptions): DynamicModule {
        return {
            module: NbSecurityModule,
            providers: [
                {provide: NB_SECURITY_OPTIONS_TOKEN, useValue: nbSecurityOptions},
                NbAclService,
            ],
            exports: [NbAclService]
        };
    }
}
