import {JwtModule} from '@nestjs/jwt';
import {Global, HttpModule, Module} from '@nestjs/common';

import {ConfigService} from './shared/services/config.service';
import {ValidatorService} from './shared/services/validator.service';
import {AwsS3Service} from './shared/services/aws-s3.service';
import {GeneratorService} from './shared/services/generator.service';
import {NbSecurityModule} from "./security/security.module";
import {securityConfig} from "@mikalair/security";

const providers = [ConfigService, ValidatorService, AwsS3Service, GeneratorService];

@Global()
@Module({
    providers,
    imports: [
        HttpModule,
        NbSecurityModule.forRoot(securityConfig),
        JwtModule.registerAsync({
            imports: [
                SharedModule,
            ],
            useFactory: (configService: ConfigService) => {
                return {
                    secretOrPrivateKey: configService.get('JWT_SECRET_KEY'),
                    // if you want to use token with expiration date
                    // signOptions: {
                    //     expiresIn: configService.getNumber('JWT_EXPIRATION_TIME'),
                    // },
                };
            },
            inject: [
                ConfigService,
            ],
        })],
    exports: [...providers, HttpModule, JwtModule, NbSecurityModule],
})
export class SharedModule {
}
