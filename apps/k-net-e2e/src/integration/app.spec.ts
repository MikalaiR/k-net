import { getGreeting } from '../support/app.po';

describe('k-net', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to k-net!');
  });
});
