module.exports = {
  name: 'k-net',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/k-net',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
