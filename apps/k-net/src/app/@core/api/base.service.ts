import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {RequestQueryBuilder} from "@nestjsx/crud-request";

export class BaseAPIService<T> {

  constructor(private _entityName: string, protected http: HttpClient) { }

  getMany(query?: RequestQueryBuilder): Observable<T[]> {

    if (!query) {
      query = RequestQueryBuilder.create();
    }

    return this.http.get<T[]>(`${this._entityName}?${query.query()}`);
  }

  getOne(id: number, query?: RequestQueryBuilder): Observable<T> {
    if (!query) {
      query = RequestQueryBuilder.create();
    }
    return this.http.get<T>(`${this._entityName}/${id}?${query.query()}`);
  }

  createOne(entity: T): Observable<T[]> {
      return this.http.post<T[]>(`${this._entityName}`, entity);
  }

  updateOne(id: number, entity: T): Observable<T[]> {
    return this.http.post<T[]>(`${this._entityName}/${id}`, entity);
  }
}
