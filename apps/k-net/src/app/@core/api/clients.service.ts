import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BaseAPIService} from "./base.service";

@Injectable({
  providedIn: 'root'
})
export class ClientsService extends BaseAPIService<{}>{
  constructor(private _http: HttpClient) {
    super('clients', _http);
  }
}
