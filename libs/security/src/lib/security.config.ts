export const securityConfig = {
  accessControl: {
    guest: {
      view: ['tariffs', 'services']
    },
    manager: {
      parent: 'guest',
      view: ['clients', 'contracts', 'transactions', 'sessions'],
      edit: ['clients', 'contracts'],
      create: ['clients', 'contracts']
    },
    admin: {
      parent: 'manager',
      view: '*',
      edit: '*',
      create: '*'
    }
  }
};
